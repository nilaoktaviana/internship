<?php


namespace lala\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 Example class MainController

 @package LalaProject
 @subpackage MainController
 @author Lala <nila.octav@gmail.com>
*/

class MainController {
    
    /**
		Example function of satu

		@return Return Message 
    */
    public function satu() {
        return new Response('Message : satu');
    }

	/**
		Example function of dua
		@return Return Message 
    */
    public function dua(){
    	return new Response('Message : dua');
    }

	/**
		Example function of tiga

		@return Return Message 
    */
    public function tiga(){
    	return new Response('Message : tiga');
    }

    /**
		Example function of empat

		@return Return Message 
    */
    public function empat(){
    	return new Response('Message : empat');
    }

    /**
		Example function of lima

		@return Return Message 
    */
    public function lima(){
    	return new Response('Message : lima');
    }
    
}

