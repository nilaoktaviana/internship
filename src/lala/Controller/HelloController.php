<?php


namespace lala\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 Example class HelloController

 @package LalaProject
 @subpackage HelloController
 @author Lala <nila.octav@gmail.com>
*/

class HelloController {
    
    /**
		Example function of index

		@return Return Message 
    */
    public function index() {
            return new Response('Message : Hello LALA');
    }

	/**
		Example function of tambah

		@return Return Message 
    */
    public function tambah(){
    	return new Response('Message : 1 + 1 = 2');
    }

	/**
		Example function of kurang

		@return Return Message 
    */
    public function kurang(){
    	return new Response('Message : 1 - 1 = 0');
    }

    /**
		Example function of kali

		@return Return Message 
    */
    public function kali(){
    	return new Response('Message : 1 * 1 = 1');
    }

    /**
		Example function of bagi

		@return Return Message 
    */
    public function bagi(){
    	return new Response('Message : 1 / 1 = 1');
    }
    
}

