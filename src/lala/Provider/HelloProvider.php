<?php 

namespace lala\Provider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

/**
 an example class HelloProvider

 @package LalaProject
 @subpackage HelloController
 @author Lala <nila.octav@gmail.com>
*/
class HelloProvider implements ControllerProviderInterface {

    public function connect(Application $app) {
        $hello = $app["controllers_factory"];
        $hello->get("/", "lala\\Controller\\HelloController::index");
        $hello->get("/", "lala\\Controller\\HelloController::tambah");
        $hello->get("/", "lala\\Controller\\HelloController::kurang");
        
        return $hello;
    }


}
